public class ArrayLapto {
    public static void main(String[] args) {
        Laptop[] kumpulanLaptop = new Laptop[3];

        kumpulanLaptop[0] = new Laptop("Dell", "XPS13", 3, "Windows", 5000000) ;
        kumpulanLaptop[1] = new Laptop("Lenovo", "Legion", 4, "Windows", 4000000);
        kumpulanLaptop[2] = new Laptop("Apple","Macbook", 4, "MacOS", 9000000);

        for (Laptop x : kumpulanLaptop) {
            System.out.println(x.getMerk());
            System.out.println(x.getTipe());
            System.out.println(x.getGPUSpeed());
            System.out.println(x.getSistem_Operasi());
            System.out.println(x.getHarga());
        }
    }
}
