import java.util.Scanner;

public class Calculator {

    private int pilihan;
    private int angka1;
    private int angka2;
    private int hasil;
    private double hasil2;

    Scanner input = new Scanner(System.in);

    void startCalculator(){
        System.out.println("Menu : \n1. Penjumlahan \n2. Pengurangan \n3. Perkalian \n4. Pembagian");
        System.out.print("Pilihan : ");
        this.pilihan = input.nextInt();
        System.out.print("Masukkan nilai pertama : ");
        this.angka1 = input.nextInt();
        System.out.print("Masukkan nilai kedua : ");
        this.angka2 = input.nextInt();

        checkopsi(this.pilihan);
    }

    void checkopsi(int pilihan){
        if (pilihan == 1) {
            hasil = penjumlahan(this.angka1, this.angka2);
            System.out.println("Hasil penjumlahan : " + hasil);
        }
        else if (pilihan == 2) {
            hasil = pengurangan(this.angka1, this.angka2);
            System.out.println("Hasil pengurangan : " + hasil);
        }
        else if (pilihan == 3) {
            hasil = perkalian(this.angka1,this.angka2);
            System.out.println("Hasil perkalian : " + hasil);
        }
        else if (pilihan == 4) {
            hasil2 = pembagian(this.angka1,this.angka2);
            System.out.println("Hasil pembagian : " + hasil2);
        }
    }

    int penjumlahan(int angka1, int angka2) {
        hasil = angka1 + angka2;
        return hasil;
    }

    int pengurangan(int angka1, int angka2) {
        hasil = angka1 - angka2;
        return hasil;
    }

    int perkalian(int angka1, int angka2){
        hasil = angka1 * angka2;
        return hasil;
    }

    double pembagian(int angka1, int angka2){
        hasil2 = (double) angka1 / angka2;
        return hasil2;
    }
}
