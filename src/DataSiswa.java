public class DataSiswa {

    private String nama;
    private int[] kumpulanNilai;
    private int rata;
    private String konvert;
    private int sums = 0;
    private int antrian;

    DataSiswa(){
    }

    DataSiswa(String nama, int antrian) {
        setNama(nama);
        setAntrian(antrian);
    }

    DataSiswa(String nama, int[] kumpulanNilai){
        setNama(nama);
        setKumpulanNilai(kumpulanNilai);
        total(kumpulanNilai);
        rerata(sums);
        konvert(rata);
    }

    int total(int[] kumpulanNilai){
        for(int i : kumpulanNilai){
            sums += i;
        }
        return sums;
    }

    int rerata(int sums){
        rata = sums / kumpulanNilai.length;
        return rata;
    }

    String konvert(int rata) {
        if (rata >= 90) {
            konvert = "A";
            return konvert;
        }
        else if (rata >= 70) {
            konvert = "B";
            return konvert;
        }
        else if (rata >= 50) {
            konvert = "C";
            return konvert;
        }
        else {
            konvert = "E";
            return konvert;
        }
    }

    public void setAntrian(int antrian) {
        this.antrian = antrian;
    }

    public int getAntrian() {
        return antrian;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setKumpulanNilai(int[] kumpulanNilai) {
        this.kumpulanNilai = kumpulanNilai;
    }

    public void setSums(int sums) {
        this.sums = sums;
    }

    public void setRata(int rata) {
        this.rata = rata;
    }

    public void setKonvert(String konvert) {
        this.konvert = konvert;
    }



    public String getNama() {
        return nama;
    }

    public int[] getKumpulanNilai() {
        return kumpulanNilai;
    }

    public int getSums() {
        return sums;
    }

    public int getRata() {
        return rata;
    }

    public String getKonvert() {
        return konvert;
    }




}
