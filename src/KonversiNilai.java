import java.util.Scanner;

public class KonversiNilai {

    static Scanner input = new Scanner(System.in);

    private int banyakBilangan;
    private int hasil;
    private int rata;

    KonversiNilai(int banyakBilangan){
        setBanyakBilangan(banyakBilangan);
    }

    public void setBanyakBilangan(int banyakBilangan) {
        this.banyakBilangan = banyakBilangan;
    }

    public int getBanyakBilangan() {
        return banyakBilangan;
    }

    int penjumlahan(){
        for (int i = 1; i <= banyakBilangan; i++){
            System.out.print("Masukkan nilai " + i + " : ");
            int nilai = input.nextInt();
            hasil += nilai;
        }
        System.out.println("Jumlah Nilai = " + hasil);
        return hasil;
    }

    void konversi(){
        rata = hasil / banyakBilangan;

        if (rata >= 90) {
            System.out.println("Nilai anda A");
        }
        else if (rata >= 70) {
            System.out.println("Nilai anda B");
        }
        else if (rata >= 0){
            System.out.println("Niali anda C");
        }
    }
}
