public class Laptop {

    private String merk;
    private String tipe;
    private  double GPUSpeed;
    private String Sistem_Operasi;
    private int harga;

    Laptop(String merk, String tipe, double GPUSpeed, String Sistem_Operasi, int harga){
        /*this.merk = merk;
        this.tipe = tipe;
        this.GPUSpeed = GPUSpeed;
        this.Sistem_Operasi = Sistem_Operasi;
        this.harga = harga;*/
        setMerk(merk);
        setTipe(tipe);
        setGPUSpeed(GPUSpeed);
        setSistem_Operasi(Sistem_Operasi);
        setHarga(harga);

    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public void setGPUSpeed(double GPUSpeed) {
        this.GPUSpeed = GPUSpeed;
    }

    public void setSistem_Operasi(String sistem_Operasi) {
        Sistem_Operasi = sistem_Operasi;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getMerk() {
        return merk;
    }

    public String getTipe() {
        return tipe;
    }

    public double getGPUSpeed() {
        return GPUSpeed;
    }

    public String getSistem_Operasi() {
        return Sistem_Operasi;
    }

    public int getHarga() {
        return harga;
    }




}

