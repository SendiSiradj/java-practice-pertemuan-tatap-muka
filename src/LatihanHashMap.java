import java.util.HashMap;
import java.util.Scanner;

public class LatihanHashMap {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        /*ashMap<String, Integer> nilaiSiswa = new HashMap<>();

        nilaiSiswa.put("Sendi", 100);
        nilaiSiswa.put("Ilham", 90);
        nilaiSiswa.put("Aldi", 80);

        for (String x : nilaiSiswa.keySet()) {
            System.out.print(x);
            System.out.print(" ");
            System.out.println(nilaiSiswa.get(x));
        }*/


        System.out.print("Masukkan banyak siswa : ");
        int banyakSiswa = Integer.parseInt(input.nextLine());
        HashMap<Integer, DataSiswa> dataSiswa = new HashMap<>();

        for (int i = 0; i < banyakSiswa; i++){
            System.out.print("Masukkan Nama Siswa : ");
            String namaSiswa = input.nextLine();

            System.out.print("Masukkan Banyak Nilai : ");
            int banyakNilai = Integer.parseInt(input.nextLine());

            int[] nilaiSiswa = new int[banyakNilai];

            for (int j = 0; j < banyakNilai; j++){
                System.out.print("Masukkan Nilai ke " + (j+1) + " : ");
                nilaiSiswa[j] = Integer.parseInt(input.nextLine());
            }
            dataSiswa.put(1706043771 + i, new DataSiswa(namaSiswa, nilaiSiswa));
        }

        for (Integer x: dataSiswa.keySet()) {
            System.out.print(x + " ");
            DataSiswa temp = dataSiswa.get(x);
            System.out.print(temp.getNama() + "\t");
            for (int Nilai : temp.getKumpulanNilai()) {
                System.out.print(Nilai + "\t");
            }
            System.out.print(temp.getSums() + "\t");
            System.out.print(temp.getRata() + "\t");
            System.out.print(temp.getKonvert() + "\t");
            System.out.println();

        }


    }
}

