import java.util.LinkedList;
import java.util.Queue;

public class LatihanQueue1 {

    public static void main(String[] args) {
        Queue<DataSiswa> data1 = new LinkedList<>();
        data1.add(new DataSiswa("Sendi", 1));
        data1.add(new DataSiswa("Alpi", 2));
        data1.add(new DataSiswa("Iori", 3));

        for (DataSiswa x : data1) {
            System.out.println(x.getNama());
        }
    }
}
