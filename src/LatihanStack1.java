import java.util.Stack;

public class LatihanStack1 {

    public static void main(String[] args) {

        Stack<Laptop> stack = new Stack<>();
        Laptop dell = new Laptop("Dell", "XPS", 4.0,"Windows 10", 10000000);
        stack.push(dell);
        for (Laptop x: stack) {
            System.out.println(x.getTipe());

        }
    }
}
