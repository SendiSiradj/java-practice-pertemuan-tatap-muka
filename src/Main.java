import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	 /*
            Menu :
            1. Penjumlahan
            2. Pengurangan
            3. Perkalian
            4. Pembagian
            Pilihan : 2
            Masukkan Nilai Pertama : 10
            Masukkan Nilai Kedua : 20
            Pengurangan = -10
         */
        System.out.println("Menu : \n1. Penjumlahan \n2. Pengurangan \n3. Perkalian \n4. Pembagian");
        Scanner input = new Scanner(System.in);
        System.out.print("Pilihan : ");
        int pilihan = input.nextInt();
        int angka1;
        int angka2;
            //selama angka masih salah maka input akan diulang
            do {
                System.out.print("Masukkan Nilai Pertama : ");
                angka1 = input.nextInt();
            }
            while(angka1 > 100);

            do {
                System.out.print("Masukkan Nilai Kedua : ");
                angka2 = input.nextInt();
            }
            while(angka2 > 100);



                //buat conditional if lagi.

                if (pilihan == 1) {
                    int hasil = angka1 + angka2;
                    System.out.println("Penambahan : " + hasil);
                } else if (pilihan == 2) {
                    int hasil = angka1 - angka2;
                    System.out.println("Pengurangan : " + hasil);
                } else if (pilihan == 3) {
                    int hasil = angka1 * angka2;
                    System.out.println("Perkalian : " + hasil);
                } else if (pilihan == 4) {
                    double hasil = (double) angka1 / angka2;
                    System.out.println("Pembagian : " + hasil);
                } else {
                    System.out.println("Pilihan tidak ada di menu");
                }




    }
}
