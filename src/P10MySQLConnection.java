import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class P10MySQLConnection {

    private static final String DATABASE_DRIVER =
            "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL =
            "jdbc:mysql://localhost:3306/db_latihan";
    private static final String USERNAME =
            "root";
    private static final String PASSWORD =
            "";
    private static final String MAX_POOL =
            "250";

    private Connection connection;
    private Properties properties;

    Connection connectionOpen(){
        if(connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
                System.out.println("Connection Established");
            } catch (Exception error) {
                System.out.println("getConnection : "+error.getMessage());
            }
        }
        return connection;
    }

    void connectionClose(){
        if(connection != null) {
            try{
                connection.close();
                connection = null;
                System.out.println("Connection Closed");
            } catch (Exception error) {
                System.out.println("Disconnect Fail : "+error.getMessage());
            }
        }
    }

    Properties getProperties(){
        if(properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }
}
