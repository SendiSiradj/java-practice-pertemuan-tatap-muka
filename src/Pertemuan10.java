import com.mysql.jdbc.MySQLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Pertemuan10 {

    public static void main(String[] args) {
        P10MySQLConnection mySQLConnection = new P10MySQLConnection();
        Scanner input = new Scanner(System.in);
        try {
            Connection conn = mySQLConnection.connectionOpen();
            String sqlQuery = "SELECT * FROM `tb_siswa`";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);

            ArrayList myArrayList = new ArrayList();

            while (resultSet.next()){
             /*   System.out.print(resultSet.getString("id")+"\t");
                System.out.print(resultSet.getString("nama")+"\t");
                System.out.print(resultSet.getString("jenis_kelamin")+"\t");
                System.out.print(resultSet.getString("no_telp")+"\t");
                */
                myArrayList.add(resultSet.getString("id"));
                myArrayList.add(resultSet.getString("nama"));
                myArrayList.add(resultSet.getString("jenis_kelamin"));
                myArrayList.add(resultSet.getString("no_telp"));

                for (int i = 0; i<myArrayList.size(); i++){
                    System.out.println(myArrayList.get(i));
                }

            }
        } catch (Exception error) {
            System.out.println("Ambil Data : "+error.getMessage());
        } finally {
            mySQLConnection.connectionClose();
        }


        try {
            Connection conn = mySQLConnection.connectionOpen();
            String sqlQuery = "UPDATE `tb_siswa` SET `nama` = 'Muhammad Sendi Siradj' WHERE `tb_siswa`.`id` = 1 ";
            conn.prepareStatement(sqlQuery).execute();
            System.out.println("Berhasil Merubah Data");
        } catch (Exception error) {
            System.out.println("Error Merubah Data : "+error.getMessage());

        } finally {
            mySQLConnection.connectionClose();
        }

        try {
            Connection conn = mySQLConnection.connectionOpen();
            String sqlQuery = "UPDATE `tb_siswa` SET `nama` = 'Muhammad Sendi Siradj' WHERE `tb_siswa`.`id` = 1 ";
            conn.prepareStatement(sqlQuery).execute();
            System.out.println("Berhasil Merubah Data");
        } catch (Exception error) {
            System.out.println("Error Merubah Data : "+error.getMessage());

        } finally {
            mySQLConnection.connectionClose();
        }

        try {
            System.out.println("masukkan nama : ");
            String nama = input.nextLine();
            System.out.println("Masukkan Jenis Kelamin : ");
            String jenisKelamin = input.nextLine();
            System.out.println("Masukkan no Telp : ");
            String noTelp = input.nextLine();

            Connection conn = mySQLConnection.connectionOpen();
            String sqlQuery = "INSERT INTO `tb_siswa` (`id`, `nama`, `jenis_kelamin`, `no_telp`) VALUES (NULL, '"+nama+"', '"+jenisKelamin+"', '"+noTelp+"') ";
            conn.prepareStatement(sqlQuery).execute();
            System.out.println("Berhasil Menambahkan Data");
        }catch (Exception error) {
            System.out.println("Error Tambah Data : "+error.getMessage());
        }finally {
            mySQLConnection.connectionClose();
        }

    }
}
