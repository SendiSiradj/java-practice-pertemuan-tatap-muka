import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        int size = arr.length;
        int tmp = 0;
        long min = 0;
        long max = 0;
        long tmp2 = 0;

        for (int i = 0; i < size; i++){
            for (int j = 1; j < (size - i); j++){
                if (arr[j-1] > arr[j]) {
                    tmp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        for (int x : arr){
            System.out.print(x + "\t");
        }

        for (int a = 0; a < size-1; a++){
            tmp2 = arr[a];
            min += tmp2;
        }

        for (int b = 1; b < size; b++){
            tmp2 = arr[b];
            max += tmp2;
        }
        System.out.println();
        System.out.print(min + " " + max);


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
