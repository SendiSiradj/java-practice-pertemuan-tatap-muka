import java.util.Scanner;

public class func4 {
    static Scanner input = new Scanner(System.in);
    static int x = 0;

    public static void main(String[] args) {
    do {
        inputBilangan();
        proses(x);
    }while (x != 0);

    }
    static int inputBilangan(){
        boolean benar = true;
        do {
            System.out.println("Pilih program :\n1.Kalkulator sederhana \n2.Konversi nilai \n0.Exit");
            System.out.print("Masukkan angka : ");
            x = input.nextInt();
            if (x == 0  || x == 1 || x == 2){
                benar = false;
            }
        }


        while(benar);
        return x;
    }

    static void proses(int inputBilangan){
        if (inputBilangan == 1) {
            calculator();
        }
        else if (inputBilangan == 2){
            program_penjumlahan();
        }
        else if (inputBilangan == 0){
            System.out.println("Anda telah keluar dari program");
        }
    }

    static void calculator(){
        System.out.println("Menu : \n1. Penjumlahan \n2. Pengurangan \n3. Perkalian \n4. Pembagian");
        System.out.print("Pilihan : ");
        int pilihan = input.nextInt();
        int angka1;
        int angka2;
        //selama angka masih salah maka input akan diulang
        do {
            System.out.print("Masukkan Nilai Pertama : ");
            angka1 = input.nextInt();
        }
        while(angka1 > 100);

        do {
            System.out.print("Masukkan Nilai Kedua : ");
            angka2 = input.nextInt();
        }
        while(angka2 > 100);

        if (pilihan == 1) {
            int hasil = angka1 + angka2;
            System.out.println("Penambahan : " + hasil);
        } else if (pilihan == 2) {
            int hasil = angka1 - angka2;
            System.out.println("Pengurangan : " + hasil);
        } else if (pilihan == 3) {
            int hasil = angka1 * angka2;
            System.out.println("Perkalian : " + hasil);
        } else if (pilihan == 4) {
            double hasil = (double) angka1 / angka2;
            System.out.println("Pembagian : " + hasil);
        } else {
            System.out.println("Pilihan tidak ada di menu");
        }

    }

    static void program_penjumlahan(){
        System.out.print("Masukkan banyak bilangan : ");
        int banyakBilangan = input.nextInt();
        int hasil = 0;
        for (int i = 1; i <= banyakBilangan; i++) {
            System.out.print("Masukkan Nilai " + i + " : ");
            int nilai = input.nextInt();
            hasil += nilai;
        }
        System.out.println("Jumlah Nilai = " + hasil);
        int rata = hasil / banyakBilangan;

        if (rata >= 90) {
            System.out.println("Nilai anda A");
        }
        else if (rata >= 70) {
            System.out.println("Nilai anda B");
        }
        else if (rata >= 0){
            System.out.println("Niali anda C");
        }


    }


}
